# Rama


**RAMA**

Rama est un projet d'application web. 
Il s'agit de la partie backend du projet réalisé par **Bilel ABBAS**. Il permet de gérer des utilisateurs et une liste de produits. 


L'application est programmée en java. Elle utilise SpringBoot et Maven.


Le code source a été testée par JUnit et analysé par SonarQube. Rama utilise comme base de données MySQL. 


Cette application s'utilise avec un site web, c'est la partie frontend du projet Rama (https://gitlab.com/bilel.abbas/rama-frontend.git).

