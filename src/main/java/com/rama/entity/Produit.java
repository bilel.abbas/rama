package com.rama.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author bilel
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Produit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false, unique = true)
	private String name;

	@Column(nullable = false)
	private double prix;

	@Column(nullable = false)
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "id_user")
	private User user;

	public Produit(Integer id, String name, double prix, String description) {
		super();
		this.id = id;
		this.name = name;
		this.prix = prix;
		this.description = description;
	}
	
	

}
