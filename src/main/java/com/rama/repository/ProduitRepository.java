package com.rama.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.rama.entity.Produit;

/**
 * @author bilel
 *
 */
@Repository
public interface ProduitRepository extends JpaRepository<Produit, Integer> {

	public Produit readByName(String name);

	@Transactional
	@Modifying
	@Query(value = "select * from produit order by prix desc", nativeQuery = true)
	public List<Produit> readByPriceDesc();

	@Transactional
	@Modifying
	@Query(value = "select * from produit order by prix asc", nativeQuery = true)
	public List<Produit> readByPriceAsc();
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = 
//	"SET FOREIGN_KEY_CHECKS=0 ;"
//			       + " update produit set id_user =0 ;"+
	                "update produit set id_user =:iu where id =:ip", nativeQuery = true)
	public int selectProduct(@Param(value = "iu")Integer iu,@Param(value = "ip")Integer ip);
	
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "select * from produit where id_user =:iu", nativeQuery = true)
	public List<Produit> afficherProductSelectionner(@Param(value = "iu")Integer iu);


}
