package com.rama.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rama.entity.User;

/**
 * @author bilel
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	public User readByEmailAndPassword(String email, String password);

	public User readByEmail(String email);

}
