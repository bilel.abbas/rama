package com.rama;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author bilel
 *
 */
@SpringBootApplication
public class RamaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RamaApplication.class, args);
	}

}
