package com.rama.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author bilel
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class UserCreateDto {

	@NotNull
	@ApiModelProperty(required = true, example = "ABBAS")
	private String nom;

	@NotNull
	@ApiModelProperty(required = true, example = "Bilel")
	private String prenom;

	@NotNull
	@Email
	@ApiModelProperty(required = true, example = "bilel.abbas@gmail.com")
	private String email;

	@NotNull
	@Size(min = 8)
	@ApiModelProperty(required = true, example = "azerty")
	private String password;

	@NotNull
	@ApiModelProperty(required = true, example = "Grenoble")
	private String ville;

	@NotNull
	@ApiModelProperty(required = true, example = "25")
	@Min(value = 18)
	@Max(value = 80)
	private int age;

}