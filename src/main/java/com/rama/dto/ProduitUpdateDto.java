package com.rama.dto;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author bilel
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class ProduitUpdateDto {

	@NotNull
	@ApiModelProperty(required = true, example = "1")
	private int id;

	@NotNull
	@ApiModelProperty(required = true, example = "Ordinateur")
	private String name;

	@NotNull
	@ApiModelProperty(required = true, example = "20")
	private double prix;

	@NotNull
	@ApiModelProperty(required = true, example = "Un ordinateur génial")
	private String description;

}
