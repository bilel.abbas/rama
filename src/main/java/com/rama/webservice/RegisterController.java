package com.rama.webservice;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rama.dto.UserCreateDto;

/**
 * @author bilel
 *
 */
@RequestMapping
@CrossOrigin
public interface RegisterController {

	@PostMapping(path = "register")
	public UserCreateDto register(@Valid @RequestBody UserCreateDto dto);

}
