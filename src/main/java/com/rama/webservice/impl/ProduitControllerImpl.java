package com.rama.webservice.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.rama.converter.ProduitDtoConverter;
import com.rama.dto.ProduitCreateDto;
import com.rama.dto.ProduitUpdateDto;
import com.rama.service.IProduitService;
import com.rama.webservice.ProduitController;

/**
 * @author bilel
 *
 */
@RestController
public class ProduitControllerImpl implements ProduitController {

	@Autowired
	@Qualifier("produitService")
	private IProduitService service;

	@Override
	public ProduitCreateDto create(@Valid ProduitCreateDto dto) {
		return ProduitDtoConverter.MAPPER.convertCreate(service.save(ProduitDtoConverter.MAPPER.convertCreate(dto)));
	}

	@Override
	public ProduitUpdateDto update(@Valid ProduitUpdateDto dto) {
		return ProduitDtoConverter.MAPPER.convertUpdate(service.update(ProduitDtoConverter.MAPPER.convertUpdate(dto)));
	}

	@Override
	public boolean delete(int id) {
		return service.delete(id);
	}

	@Override
	public List<ProduitUpdateDto> readAll() {
		return ProduitDtoConverter.MAPPER.convert(service.readAll());
	}

	@Override
	public List<ProduitUpdateDto> readAlldsc() {
		return ProduitDtoConverter.MAPPER.convert(service.readDesc());
	}

	@Override
	public List<ProduitUpdateDto> readAllasc() {
		return ProduitDtoConverter.MAPPER.convert(service.readAsc());
	}

	@Override
	public ProduitCreateDto add(@Valid ProduitCreateDto dto) {
		return ProduitDtoConverter.MAPPER.convertCreate(service.create(ProduitDtoConverter.MAPPER.convertCreate(dto)));
	}
	
	@Override
	public int selection(int iu,int ip) {
		return service.select(iu, ip);
	}
	
	@Override
	public List<ProduitUpdateDto> afficheSelection(int iu) {
		return ProduitDtoConverter.MAPPER.convert(service.afficherProduitSelect(iu));
	}

}
