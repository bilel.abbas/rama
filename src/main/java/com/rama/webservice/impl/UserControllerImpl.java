package com.rama.webservice.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.rama.converter.UserDtoConverter;
import com.rama.dto.UserCreateDto;
import com.rama.dto.UserUpdateDto;
import com.rama.service.IUserService;
import com.rama.webservice.UserController;

/**
 * @author bilel
 *
 */
@RestController
public class UserControllerImpl implements UserController {

	@Autowired
	@Qualifier("userService")
	private IUserService service;

	@Override
	public UserCreateDto create(@Valid UserCreateDto dto) {
		return UserDtoConverter.MAPPER.convertCreate(service.save(UserDtoConverter.MAPPER.convertCreate(dto)));
	}

	@Override
	public UserUpdateDto update(@Valid UserUpdateDto dto) {
		return UserDtoConverter.MAPPER.convertUpdate(service.update(UserDtoConverter.MAPPER.convertUpdate(dto)));
	}

	@Override
	public boolean delete(int id) {
		return service.delete(id);
	}

	@Override
	public List<UserUpdateDto> readAll() {
		return UserDtoConverter.MAPPER.convert(service.readAll());
	}

	@Override
	public UserUpdateDto readById(int id) {
		return UserDtoConverter.MAPPER.convertUpdate(service.readById(id));
	}

	@Override
	public UserUpdateDto showByEmail(String email) {
		return UserDtoConverter.MAPPER.convertUpdate(service.searchById(email));
	}

}
