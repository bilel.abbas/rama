package com.rama.webservice.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.rama.converter.UserDtoConverter;
import com.rama.dto.UserCreateDto;
import com.rama.service.IUserService;
import com.rama.webservice.RegisterController;

/**
 * @author bilel
 *
 */
@RestController
public class RegisterControllerImpl implements RegisterController {

	@Autowired
	@Qualifier("userService")
	private IUserService service;

	@Override
	public UserCreateDto register(@Valid UserCreateDto dto) {
		return UserDtoConverter.MAPPER.convertCreate(service.register(UserDtoConverter.MAPPER.convertCreate(dto)));
	}

}
