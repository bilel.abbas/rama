package com.rama.webservice.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rama.converter.UserDtoConverter;
import com.rama.dto.UserLoginDto;
import com.rama.service.IUserService;
import com.rama.webservice.LoginController;

/**
 * @author bilel
 *
 */
@RestController
public class LoginControllerImpl implements LoginController {

	@Autowired
	@Qualifier("userService")
	private IUserService service;

	@Override
	public UserLoginDto login(@Valid @RequestBody UserLoginDto dto) {
		return UserDtoConverter.MAPPER.convertLogin(service.login(UserDtoConverter.MAPPER.convertLogin(dto).getEmail(),
				UserDtoConverter.MAPPER.convertLogin(dto).getPassword()));
	}

}
