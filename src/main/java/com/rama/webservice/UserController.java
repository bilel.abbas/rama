package com.rama.webservice;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rama.dto.UserCreateDto;
import com.rama.dto.UserUpdateDto;

/**
 * @author bilel
 *
 */
@RequestMapping
@CrossOrigin
public interface UserController {

	@PostMapping(path = "create")
	public UserCreateDto create(@Valid @RequestBody UserCreateDto dto);

	@PutMapping(path = "update")
	public UserUpdateDto update(@Valid @RequestBody UserUpdateDto dto);

	@DeleteMapping(path = "delete/{id}/")
	public boolean delete(@PathVariable(name = "id") int id);

	@GetMapping(path = "get")
	public List<UserUpdateDto> readAll();

	@GetMapping(path = "get/{id}/")
	public UserUpdateDto readById(@PathVariable(name = "id") int id);

	@GetMapping(path = "show/{email}/")
	public UserUpdateDto showByEmail(@PathVariable(name = "email") String email);

}
