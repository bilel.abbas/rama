package com.rama.webservice;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rama.dto.ProduitCreateDto;
import com.rama.dto.ProduitUpdateDto;

/**
 * @author bilel
 *
 */
@RequestMapping
@CrossOrigin
public interface ProduitController {

	@PostMapping(path = "product/create")
	public ProduitCreateDto create(@Valid @RequestBody ProduitCreateDto dto);

	@PostMapping(path = "product/add")
	public ProduitCreateDto add(@Valid @RequestBody ProduitCreateDto dto);

	@PutMapping(path = "product/update")
	public ProduitUpdateDto update(@Valid @RequestBody ProduitUpdateDto dto);

	@DeleteMapping(path = "product/delete/{id}/")
	public boolean delete(@PathVariable(name = "id") int id);

	@GetMapping(path = "product/get")
	public List<ProduitUpdateDto> readAll();

	@GetMapping(path = "product/get/desc")
	public List<ProduitUpdateDto> readAlldsc();

	@GetMapping(path = "product/get/asc")
	public List<ProduitUpdateDto> readAllasc();
	
	@PutMapping(path = "product/{iu}/select/{ip}/")
	public int selection(@PathVariable(name = "iu")int iu,@PathVariable(name = "ip")int ip);
	
	@GetMapping(path = "product/{iu}/afficheSelect/")
	public List<ProduitUpdateDto> afficheSelection(@PathVariable(name = "iu")int iu);

}
