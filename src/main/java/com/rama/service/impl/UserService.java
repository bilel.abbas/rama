package com.rama.service.impl;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.rama.entity.User;
import com.rama.repository.UserRepository;
import com.rama.service.IUserService;

/**
 * @author bilel
 *
 */
@Service
public class UserService implements IUserService {

	@Autowired
	private UserRepository repo;

	/**
	 * Methode create; La méthode permet de sauvegarder un utilisateur.
	 * 
	 * @param
	 * @return Retourne l'utilisateur si il existe sinon elle retourne null.
	 */
	@Override
	public User save(User user) {
		if (repo.exists(Example.of(user))) {
			return null;
		}
		return repo.save(user);

	}

	/**
	 * Methode update; La méthode permet de modifier un utilisateur.
	 * 
	 * @param
	 * @return La méthode retourne un utilisateur si l'utilisateur existe sinon elle
	 *         retourne null.
	 */
	@Override
	public User update(User user) {
		if (repo.existsById(user.getId())) {
			return repo.save(user);
		}
		return null;
	}

	/**
	 * La méthode delete; Elle permet de supprimer un utilisateur
	 * 
	 * @param
	 * @return Elle retourne true si l'utilisateur a été supprimé sinon elle
	 *         retourne false.
	 */
	@Override
	public boolean delete(int id) {

		if (repo.existsById(id)) {
			repo.deleteById(id);
			return true;
		}
		return false;

	}

	/**
	 * La méthode readAll; Elle permet d'afficher tout les utilisateurs.
	 * 
	 * @param
	 * @return Elle retourne une liste.
	 */
	@Override
	public List<User> readAll() {
		return repo.findAll();
	}

	/**
	 * La méthode login ; Elle permet de se connecter grace à un email et un mot de
	 * passe.
	 * 
	 * @param
	 * @return Si l'email et le mot de passe correspondent à un utilisateur, la
	 *         méthode renvoie un utilisateur sinon elle retourne null.
	 */
	@Override
	public User login(String email, String password) {
		return repo.readByEmailAndPassword(email, password);

	}

	/**
	 * La méthode readByEmail; Elle permet d'afficher un utilisateur relié à un
	 * email.
	 * 
	 * @param
	 * @return La méthode retourne un email si l'utilisateur existe sinon elle
	 *         retourne null.
	 */
	@Override
	public User searchById(String email) {
		return repo.readByEmail(email);
	}

	/**
	 * La méthode register; Elle permet d'enregister un utilisateur.
	 * 
	 * @param
	 * @returns La méthode enregistre un utilisateur si il n'y a pas d'email
	 *          enregistré sinon elle retourne null.
	 */
	@Override
	public User register(User user) {
		if (repo.readByEmail(user.getEmail()) == null) {
			return repo.save(user);
		}
		return null;

	}

	/**
	 * La méthode readById; Cette methode permet d'afficher un utilisateur selon son
	 * id.
	 * 
	 * @param
	 * @returns La méthode retourne un utilisateur si l'utilisateur existe sinon
	 *          elle retourne null.
	 */
	@Override
	public User readById(int id) {
		try {
			return repo.findById(id).get();
		} catch (NoSuchElementException e) {

			return null;
		}
	}

}
