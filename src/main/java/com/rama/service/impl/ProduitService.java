package com.rama.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.rama.entity.Produit;
import com.rama.repository.ProduitRepository;
import com.rama.service.IProduitService;

/**
 * @author bilel
 *
 */
@Service
public class ProduitService implements IProduitService {

	@Autowired
	private ProduitRepository repo;

	/**
	 * La méthode save : Elle permet de sauvegarder un produit.
	 * 
	 * @param
	 * @returns La méthode save renvoie un produit si il n'existe pas sinon il
	 *          renvoie un null.
	 */
	@Override
	public Produit save(Produit produit) {
		if (repo.exists(Example.of(produit))) {
			return null;
		}
		return repo.save(produit);
	}

	/**
	 * La méthode create : Elle permet de creer un produit.
	 * 
	 * @param
	 * @returns La méthode renvoie un produit si le nom n'existe pas sinon il
	 *          renvoie un null.
	 */
	@Override
	public Produit create(Produit produit) {
		if (repo.readByName(produit.getName()) == null) {
			return repo.save(produit);
		}
		return null;
	}

	/**
	 * La méthode update. Elle permet de modifier un produit.
	 * 
	 * @param
	 * @returns La méthode renvoie un produit si il existe sinon il renvoie un null.
	 *
	 */
	@Override
	public Produit update(Produit produit) {
		if (repo.existsById(produit.getId())) {
			return repo.save(produit);
		}
		return null;
	}

	/**
	 * La méthode readAll: Elle permet d'afficher tout les produits
	 * 
	 * @param
	 * @returns La méthode renvoie la liste des produits
	 */
	@Override
	public List<Produit> readAll() {
		return repo.findAll();
	}

	/**
	 * La méthode readDesc. Elle permet d'afficher un liste de produits par prix
	 * decroissant.
	 * 
	 * @param
	 * @returns La liste des produits par prix decroissant.
	 */
	@Override
	public List<Produit> readDesc() {
		return repo.readByPriceDesc();
	}

	/**
	 * La méthode delete. Elle permet de supprimer un produit selon son id.
	 * 
	 * @param
	 * @returns Elle renvoie true si le produit a été supprimé sinon elle renvoie
	 *          false.
	 */
	@Override
	public boolean delete(int id) {
		if (repo.existsById(id)) {
			repo.deleteById(id);
			return true;
		}
		return false;
	}

	/**
	 * La méthode searchProduit. Elle permet de chercher un produit selon son nom.
	 * 
	 * @param
	 * @return Si le produit existe, la méthode renvoie true. Sinon, elle renvoie
	 *         false.
	 *
	 */
	@Override
	public boolean searchProduit(String name) {
		if (repo.readByName(name) == null) {
			return false;
		}
		return true;
	}

	/**
	 * La méthode readAsc. Elle permet de renvoie une liste de produits par prix
	 * croissant.
	 * 
	 * @param
	 * @returns La méthode renvoie une liste de produits par prix croissant.
	 *
	 */
	@Override
	public List<Produit> readAsc() {
		return repo.readByPriceAsc();
	}

	/**
	 * La méthode select. Elle permet de selectionner un produit. La méthode ajoute
	 * une clé étrangère identique à l'id de l'utilisateur qui selectionne
	 * l'article.
	 * 
	 * @param
	 * @returns Elle retourne 1 si l'id_user a été modifié sinon elle retourne 0.
	 *
	 */
	@Override
	public int select(int iu, int ip) {
		return repo.selectProduct(iu, ip);
	}

	/**
	 * La méthode afficherProduitSelect. Elle permet d'afficher les produits
	 * selectionnes par l'utilisateur.
	 * 
	 * @param
	 * @returns La méthode renvoie une liste de produits
	 *
	 */
	@Override
	public List<Produit> afficherProduitSelect(int iu) {
		return repo.afficherProductSelectionner(iu);
	}

}
