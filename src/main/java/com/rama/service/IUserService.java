package com.rama.service;

import java.util.List;

import com.rama.entity.User;

/**
 * @author bilel
 *
 */
public interface IUserService {

	public User save(User user);

	public User update(User user);

	public boolean delete(int id);

	public List<User> readAll();

	public User login(String email, String password);

	public User searchById(String email);

	public User register(User user);

	public User readById(int id);

}
