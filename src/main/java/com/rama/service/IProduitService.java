package com.rama.service;

import java.util.List;

import com.rama.entity.Produit;

/**
 * @author bilel
 *
 */
public interface IProduitService {

	public Produit save(Produit produit);

	public Produit create(Produit produit);

	public boolean searchProduit(String name);

	public Produit update(Produit produit);

	public List<Produit> readAll();

	public List<Produit> readDesc();

	public List<Produit> readAsc();

	public boolean delete(int id);
	
	public int select(int iu,int ip);
	
	public List<Produit> afficherProduitSelect(int iu);

}
