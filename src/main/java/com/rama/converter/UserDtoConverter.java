package com.rama.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.rama.dto.UserCreateDto;
import com.rama.dto.UserLoginDto;
import com.rama.dto.UserUpdateDto;
import com.rama.entity.User;

/**
 * @author bilel
 *
 */
@Mapper
public interface UserDtoConverter {
	
	
	UserDtoConverter MAPPER = Mappers.getMapper(UserDtoConverter.class);
	
	User convertCreate(UserCreateDto dto);
	UserCreateDto convertCreate(User user);
	
	User convertUpdate(UserUpdateDto dto);
	UserUpdateDto convertUpdate(User user);
	
	User convertLogin(UserLoginDto dto);
	UserLoginDto convertLogin(User user);
	
	List<User> convert(ArrayList<UserUpdateDto> dto);
	List<UserUpdateDto> convert(List<User> user);


}
