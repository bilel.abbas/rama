package com.rama.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.rama.dto.ProduitCreateDto;
import com.rama.dto.ProduitUpdateDto;
import com.rama.entity.Produit;

/**
 * @author bilel
 *
 */
@Mapper
public interface ProduitDtoConverter {

	ProduitDtoConverter MAPPER = Mappers.getMapper(ProduitDtoConverter.class);

	Produit convertCreate(ProduitCreateDto dto);

	ProduitCreateDto convertCreate(Produit produit);

	Produit convertUpdate(ProduitUpdateDto dto);

	ProduitUpdateDto convertUpdate(Produit produit);

	List<Produit> convert(ArrayList<ProduitUpdateDto> dto);

	List<ProduitUpdateDto> convert(List<Produit> produit);

}
