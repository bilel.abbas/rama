package com.rama.webserviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.web.util.NestedServletException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.rama.RamaApplicationTests;
import com.rama.dto.ProduitCreateDto;
import com.rama.dto.ProduitUpdateDto;

/**
 * @author bilel
 *
 */
public class ProduitWabServiceImplTest extends RamaApplicationTests {

	@Test
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveProduitNotExistingShouldReturn200StatusandUserResponse() throws JsonProcessingException, Exception {

		ProduitCreateDto dto = new ProduitCreateDto("Ordinateur", 250, "Un produit parfait");

		String bodyAsJson = mvc
				.perform(post("/product/create").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		ProduitCreateDto response = mapper.readValue(bodyAsJson, ProduitCreateDto.class);

		assertNotNull(response);
		assertEquals("Ordinateur", response.getName());
		assertEquals(250, response.getPrix());
		assertEquals("Un produit parfait", response.getDescription());

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveProduitExistingShouldReturnNull() throws JsonProcessingException, Exception {

		ProduitCreateDto dto = new ProduitCreateDto("Ordinateur", 250, "Un produit parfait");

		String bodyAsJson = mvc
				.perform(post("/product/create").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("", bodyAsJson);

	}

	@Test
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createProduitNotExistingShouldReturnProduit() throws JsonProcessingException, Exception {

		ProduitCreateDto dto = new ProduitCreateDto("Ordinateur", 250, "Un produit parfait");

		String bodyAsJson = mvc
				.perform(post("/product/add").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		ProduitCreateDto response = mapper.readValue(bodyAsJson, ProduitCreateDto.class);

		assertNotNull(response);
		assertEquals("Ordinateur", response.getName());
		assertEquals(250, response.getPrix());
		assertEquals("Un produit parfait", response.getDescription());

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createProduitExistingShouldReturnNull() throws JsonProcessingException, Exception {

		ProduitCreateDto dto = new ProduitCreateDto("Ordinateur", 250, "Un produit parfait");

		String bodyAsJson = mvc
				.perform(post("/product/add").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("", bodyAsJson);

	}

	@Test
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateProduitExistingShouldReturnProduit() throws JsonProcessingException, Exception {

		ProduitUpdateDto dto = new ProduitUpdateDto(1, "Ordinateur", 250, "Un produit parfait");

		String bodyAsJson = mvc
				.perform(put("/product/update").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("", bodyAsJson);

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateProduitNotExistingShouldReturnNull() throws JsonProcessingException, Exception {

		ProduitUpdateDto dto = new ProduitUpdateDto(1, "Aspirateur", 350, "Un aspirateur parfait");

		String bodyAsJson = mvc
				.perform(put("/product/update").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		ProduitUpdateDto response = mapper.readValue(bodyAsJson, ProduitUpdateDto.class);

		assertEquals(1, response.getId());
		assertEquals("Aspirateur", response.getName());
		assertEquals(350, response.getPrix());
		assertEquals("Un aspirateur parfait", response.getDescription());

	}

	@Test
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteProduitNotExistingShouldReturnFalse() throws JsonProcessingException, Exception {

		int id = 1;

		String bodyAsJson = mvc.perform(delete("/product/delete/{id}/", id).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteProduitExistingShouldReturnTrue() throws JsonProcessingException, Exception {

		int id = 1;

		String bodyAsJson = mvc.perform(delete("/product/delete/{id}/", id).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", bodyAsJson);

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllProduitExistingShouldReturnListProduit() throws JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get("/product/get").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		List<ProduitUpdateDto> response = mapper.readValue(bodyAsJson, new TypeReference<List<ProduitUpdateDto>>() {
		});

		assertNotNull(response);
		assertEquals("Ordinateur", response.get(0).getName());
		assertEquals(250, response.get(0).getPrix());
		assertEquals("Un produit parfait", response.get(0).getDescription());

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait'),(2,'Aspirateur',13,'Un aspirateur occasion')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllProduitExistingShouldReturnListProduitByDesc() throws JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get("/product/get/desc").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		List<ProduitUpdateDto> response = mapper.readValue(bodyAsJson, new TypeReference<List<ProduitUpdateDto>>() {
		});

		assertNotNull(response);
		assertEquals(1, response.get(0).getId());
		assertEquals("Ordinateur", response.get(0).getName());
		assertEquals(250, response.get(0).getPrix());
		assertEquals("Un produit parfait", response.get(0).getDescription());

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait'),(2,'Aspirateur',13,'Un aspirateur occasion')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllProduitExistingShouldReturnListProduitByAsc() throws JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get("/product/get/asc").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		List<ProduitUpdateDto> response = mapper.readValue(bodyAsJson, new TypeReference<List<ProduitUpdateDto>>() {
		});

		assertNotNull(response);
		assertEquals(2, response.get(0).getId());
		assertEquals("Aspirateur", response.get(0).getName());
		assertEquals(13, response.get(0).getPrix());
		assertEquals("Un aspirateur occasion", response.get(0).getDescription());

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')",
			"Delete from user",
			"Insert into user (nom,prenom,email,password,age,ville) values('ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "delete from produit", "delete from user" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void selectProduitExistingShouldReturn1() throws JsonProcessingException, Exception {

		int iu = 1;
		int ip = 1;

		String bodyAsJson = mvc
				.perform(put("/product/{iu}/select/{ip}/", iu, ip).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("1", bodyAsJson);

	}

	@Test
	@Sql(statements = { "delete from produit", "Delete from user" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "delete from produit", "delete from user" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void selectProduitNotExistingShouldReturn0() throws JsonProcessingException, Exception {

		int iu = 1;
		int ip = 1;

		String bodyAsJson = mvc
				.perform(put("/product/{iu}/select/{ip}/", iu, ip).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("0", bodyAsJson);

	}
	
	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')",
			"Delete from user",
			"Insert into user (nom,prenom,email,password,age,ville) values('ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')",
			"update produit set id_user=1 where id=1"}, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "delete from produit", "delete from user" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void afficherProduitSelectShouldReturnListProduitSelected() throws JsonProcessingException, Exception {

		int iu = 1;
		
		try {
		String bodyAsJson = mvc.perform(get("/product/{iu}/afficheSelect/",iu).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		List<ProduitUpdateDto> response = mapper.readValue(bodyAsJson, new TypeReference<List<ProduitUpdateDto>>() {
		});

		assertNotNull(response);
		assertEquals(1, response.get(0).getId());
		assertEquals("Ordinateur", response.get(0).getName());
		assertEquals(250, response.get(0).getPrix());
		assertEquals("Un produit parfait", response.get(0).getDescription());
		}
		catch (NestedServletException e) {
			
		}

	}

}
