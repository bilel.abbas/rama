package com.rama.webserviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;

import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.rama.RamaApplicationTests;
import com.rama.dto.UserCreateDto;
import com.rama.dto.UserLoginDto;
import com.rama.dto.UserUpdateDto;

/**
 * @author bilel
 *
 */
public class UserWebServiceImplTest extends RamaApplicationTests {

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void registrationNewUserShouldReturn200StatusandUserResponse() throws JsonProcessingException, Exception {

		UserCreateDto dto = new UserCreateDto("ABBAS", "Bilel", "bilel.abbas@gmail.com", "12345678", "Grenoble", 25);

		String bodyAsJson = mvc
				.perform(post("/register").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		UserCreateDto response = mapper.readValue(bodyAsJson, UserCreateDto.class);

		assertNotNull(response);
		assertEquals("ABBAS", response.getNom());
		assertEquals("Bilel", response.getPrenom());
		assertEquals("bilel.abbas@gmail.com", response.getEmail());
		assertEquals("12345678", response.getPassword());
		assertEquals(25, response.getAge());
		assertEquals("Grenoble", response.getVille());

	}

	@Test
	@Sql(statements = { "Delete from user",
			"Insert into user (nom,prenom,email,password,age,ville) values('ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void registrationUserExistingShouldReturn200StatusandEmptyResponse()
			throws JsonProcessingException, Exception {

		UserCreateDto dto = new UserCreateDto("JABILOT", "Marc", "bilel.abbas@gmail.com", "azerty1234", "Lyon", 22);

		String bodyAsJson = mvc
				.perform(post("/register").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(200)).andReturn().getResponse().getContentAsString();

		assertEquals("", bodyAsJson);

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateUserNotExistingShouldReturn200StatusandEmptyResponse() throws JsonProcessingException, Exception {

		UserUpdateDto dto = new UserUpdateDto(1, "ABBAS", "Bilel", "bilel.abbas@gmail.com", "12345678", "Grenoble", 25);

		String bodyAsJson = mvc
				.perform(put("/update").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("", bodyAsJson);

	}

	@Test
	@Sql(statements = { "Delete from user",
			"Insert into user (id,nom,prenom,email,password,age,ville) values(1,'ABBAS','Bilel','bilel.abbas@gmail.com','adaming12345678',22,'Lyon')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateUserExistingShouldReturn200StatusandUserResponse() throws JsonProcessingException, Exception {

		UserUpdateDto dto = new UserUpdateDto(1, "JABILOT", "Marc", "bilel.abbas@gmail.com", "adaming12345678", "Lyon",
				22);

		String bodyAsJson = mvc
				.perform(put("/update").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		UserUpdateDto response = mapper.readValue(bodyAsJson, UserUpdateDto.class);

		assertNotNull(response);
		assertEquals("JABILOT", response.getNom());
		assertEquals("Marc", response.getPrenom());
		assertEquals("bilel.abbas@gmail.com", response.getEmail());
		assertEquals("adaming12345678", response.getPassword());
		assertEquals(22, response.getAge());
		assertEquals("Lyon", response.getVille());

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteUserNotExistingShouldReturn200StatusandEmptyResponse() throws JsonProcessingException, Exception {

		int id = 1;

		String bodyAsJson = mvc.perform(delete("/delete/{id}/", id).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);

	}

	@Test
	@Sql(statements = { "Delete from user",
			"Insert into user (id,nom,prenom,email,password,age,ville) values(1,'ABBAS','Bilel','bilel.abbas@gmail.com','adaming12345678',22,'Lyon')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteUserExistingShouldReturn200StatusandUserResponse() throws JsonProcessingException, Exception {

		int id = 1;

		String bodyAsJson = mvc.perform(delete("/delete/{id}/", id).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", bodyAsJson);

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createNewUserShouldReturn200StatusandUserResponse() throws JsonProcessingException, Exception {

		UserCreateDto dto = new UserCreateDto("ABBAS", "Bilel", "bilel.abbas@gmail.com", "12345678", "Grenoble", 25);

		String bodyAsJson = mvc
				.perform(
						post("/create").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		UserCreateDto response = mapper.readValue(bodyAsJson, UserCreateDto.class);

		assertNotNull(response);
		assertEquals("ABBAS", response.getNom());
		assertEquals("Bilel", response.getPrenom());
		assertEquals("bilel.abbas@gmail.com", response.getEmail());
		assertEquals("12345678", response.getPassword());
		assertEquals(25, response.getAge());
		assertEquals("Grenoble", response.getVille());

	}

	@Test
	@Sql(statements = { "Delete from user",
			"Insert into user (nom,prenom,email,password,age,ville) values('ABBAS','Bilel','bilel.abbas@gmail.com','adaming12345678',22,'Lyon')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createUserExistingShouldReturn200StatusandEmptyResponse() throws JsonProcessingException, Exception {

		UserCreateDto dto = new UserCreateDto("ABBAS", "Bilel", "bilel.abbas@gmail.com", "adaming12345678", "Lyon", 22);

		String bodyAsJson = mvc
				.perform(
						post("/create").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(200)).andReturn().getResponse().getContentAsString();

		assertEquals("", bodyAsJson);

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void LoginUserInexistingShouldReturn200Status() throws JsonProcessingException, Exception {

		UserLoginDto dto = new UserLoginDto("bilel.abbas@gmail.com", "azerty123456");

		String bodyAsJson = mvc
				.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("", bodyAsJson);

	}

	@Test
	@Sql(statements = { "Delete from user",
			"Insert into user (nom,prenom,email,password,age,ville) values('ABBAS','Bilel','bilel.abbas@gmail.com','12345678',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void loginUserExistingShouldReturnUserAnd200Status() throws JsonProcessingException, Exception {

		UserLoginDto dto = new UserLoginDto("bilel.abbas@gmail.com", "12345678");

		String bodyAsJson = mvc
				.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(200)).andReturn().getResponse().getContentAsString();

		UserLoginDto response = mapper.readValue(bodyAsJson, UserLoginDto.class);

		assertNotNull(response);
		assertEquals("bilel.abbas@gmail.com", response.getEmail());
		assertEquals("12345678", response.getPassword());

	}

	@Test
	@Sql(statements = { "Delete from user",
			"Insert into user (id,nom,prenom,email,password,age,ville) values(1,'ABBAS','Bilel','bilel.abbas@gmail.com','adaming12345678',22,'Lyon')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void showUserExistingShouldReturn200StatusandUserResponse() throws JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get("/get").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		List<UserUpdateDto> response = mapper.readValue(bodyAsJson, new TypeReference<List<UserUpdateDto>>() {
		});

		assertNotNull(response);
		assertEquals("ABBAS", response.get(0).getNom());
		assertEquals("Bilel", response.get(0).getPrenom());
		assertEquals("bilel.abbas@gmail.com", response.get(0).getEmail());
		assertEquals("adaming12345678", response.get(0).getPassword());
		assertEquals(22, response.get(0).getAge());
		assertEquals("Lyon", response.get(0).getVille());

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getUserShouldReturn200StatusandUserResponse() throws JsonProcessingException, Exception {

		int id = 2;

		String bodyAsJson = mvc.perform(get("/get/{id}/", id).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(200)).andReturn().getResponse().getContentAsString();

		System.out.println("Le message affiché est " + bodyAsJson);

		assertEquals("", bodyAsJson);

	}

	@Test
	@Sql(statements = { "Delete from user",
			"Insert into user (id, nom,prenom,email,password,age,ville) values(1,'ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getUserWithExistingEmailShouldReturn200StatusandEmptyResponse()
			throws JsonProcessingException, Exception {

		String email = "bilel.abbas@gmail.com";

		String bodyAsJson = mvc.perform(get("/show/{email}/", email).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(200)).andReturn().getResponse().getContentAsString();

		UserUpdateDto response = mapper.readValue(bodyAsJson, UserUpdateDto.class);

		assertNotNull(response);
		assertEquals("ABBAS", response.getNom());
		assertEquals("Bilel", response.getPrenom());
		assertEquals("bilel.abbas@gmail.com", response.getEmail());
		assertEquals("azerty", response.getPassword());
		assertEquals(25, response.getAge());
		assertEquals("Grenoble", response.getVille());

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getUserWithInExistingEmailShouldReturn200StatusandEmptyResponse()
			throws JsonProcessingException, Exception {

		String email = "bilel.abbas@gmail.com";

		String bodyAsJson = mvc.perform(get("/show/{email}/", email).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(200)).andReturn().getResponse().getContentAsString();

		assertEquals("", bodyAsJson);

	}

}
