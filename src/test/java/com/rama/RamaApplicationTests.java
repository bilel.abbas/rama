package com.rama;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author bilel
 *
 */
@SpringBootTest(classes = RamaApplication.class)
@AutoConfigureMockMvc
public class RamaApplicationTests {

	@Autowired
	protected MockMvc mvc;

	protected ObjectMapper mapper = new ObjectMapper();

	@Test
	void contextLoads() {
		assertTrue(true);
	}

}
