package com.rama.serviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.rama.RamaApplication;
import com.rama.entity.Produit;
import com.rama.service.IProduitService;

/**
 * @author bilel
 *
 */
@SpringBootTest(classes = RamaApplication.class)
public class ProduitServiceImplTest {

	@Autowired
	private IProduitService service;

	@Test
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveProduitNotExistingShouldReturnProduit() {

		Produit produit = new Produit(1, "Ordinateur", 250, "Un produit parfait");

		Produit result = service.save(produit);

		assertNotNull(result);
		assertEquals("Ordinateur", result.getName());
		assertEquals(250, result.getPrix());
		assertEquals("Un produit parfait", result.getDescription());

	}

	@Test
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveProduitNullNotExistingShouldReturnIllegalArgumentException() {

		Produit produit = null;

		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			service.save(produit);
		});

		assertEquals("probe is marked non-null but is null", exception.getMessage());

	}

	@Test
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveProduitNotExistingWithNameNullShouldDataIntegrityViolationException() {

		Produit produit = new Produit(1, null, 250, "Un produit parfait");

		Exception exception = assertThrows(DataIntegrityViolationException.class, () -> {
			service.save(produit);
		});

		assertEquals(
				"could not execute statement; SQL [n/a]; constraint [null]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement",
				exception.getMessage());

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveProduitExistingShouldReturnNull() {

		Produit produit = new Produit(1, "Ordinateur", 250, "Un produit parfait");

		Produit result = service.save(produit);

		assertNull(result);
	}

	@Test
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createProduitNotExistingShouldReturnProduit() {

		Produit produit = new Produit(1, "Ordinateur", 250, "Un produit parfait");

		Produit result = service.create(produit);

		assertNotNull(result);
		assertEquals("Ordinateur", result.getName());
		assertEquals(250, result.getPrix());
		assertEquals("Un produit parfait", result.getDescription());

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createProduitExistingShouldReturnNull() {

		Produit produit = new Produit(1, "Ordinateur", 250, "Un produit parfait");

		Produit result = service.create(produit);

		assertNull(result);
	}

	@Test
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateProduitNotExistingShouldReturnNull() {

		Produit produit = new Produit(1, "Ordinateur", 250, "Un produit parfait");

		Produit result = service.update(produit);
		assertNull(result);

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateProduitExistingShouldReturnProduit() {

		Produit produit = new Produit(1, "Aspirateur", 350, "Un aspirateur parfait");

		Produit result = service.update(produit);

		assertEquals(1, result.getId());
		assertEquals("Aspirateur", result.getName());
		assertEquals(350, result.getPrix());
		assertEquals("Un aspirateur parfait", result.getDescription());

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateProduitExistingWithNameNullShouldReturnDataIntegrityViolationException() {

		Produit produit = new Produit(1, null, 350, "Un produit parfait");

		Exception exception = assertThrows(DataIntegrityViolationException.class, () -> {
			service.update(produit);
		});

		assertEquals(
				"could not execute statement; SQL [n/a]; constraint [null]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement",
				exception.getMessage());

	}

	@Test
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByProduitNotExistingShouldReturnNull() {

		String name = "Ordinateur";

		boolean result = service.searchProduit(name);
		assertFalse(result);

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByProduitExistingShouldReturnProduit() {

		String name = "Ordinateur";

		boolean result = service.searchProduit(name);
		assertTrue(result);
	}

	@Test
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteProduitNotExistingShouldReturnFalse() {

		int id = 1;
		boolean result = service.delete(id);

		assertFalse(result);

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteProduitExistingShouldReturnTrue() {

		int id = 1;
		boolean result = service.delete(id);

		assertTrue(result);

	}

	@Test
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllProduitNotExistingShouldReturnEmptyList() {

		List<Produit> result = service.readAll();
		assertEquals("[]", result.toString());

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllProduitExistingShouldReturnListProduit() {

		List<Produit> result = service.readAll();

		assertEquals(1, result.get(0).getId());
		assertEquals("Ordinateur", result.get(0).getName());
		assertEquals(250, result.get(0).getPrix());
		assertEquals("Un produit parfait", result.get(0).getDescription());

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait'),(2,'Aspirateur',13,'Un aspirateur occasion')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllProduitExistingShouldReturnListProduitByDesc() {

		List<Produit> result = service.readDesc();

		assertEquals(1, result.get(0).getId());
		assertEquals("Ordinateur", result.get(0).getName());
		assertEquals(250, result.get(0).getPrix());
		assertEquals("Un produit parfait", result.get(0).getDescription());

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait'),(2,'Aspirateur',13,'Un aspirateur occasion')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from produit", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllProduitExistingShouldReturnListProduitByAsc() {

		List<Produit> result = service.readAsc();

		assertEquals(2, result.get(0).getId());
		assertEquals("Aspirateur", result.get(0).getName());
		assertEquals(13, result.get(0).getPrix());
		assertEquals("Un aspirateur occasion", result.get(0).getDescription());

	}

	@Test
	@Sql(statements = { "delete from produit","SET FOREIGN_KEY_CHECKS=0",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')",
			"Delete from user",
			"Insert into user (nom,prenom,email,password,age,ville) values('ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "delete from produit", "delete from user" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void selectProduitExistingShouldReturn1() {

		int iu = 1;
		int ip = 1;

		int result = service.select(iu, ip);

		assertEquals(1, result);

	}

	@Test
	@Sql(statements = { "delete from produit", "Delete from user" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "delete from produit", "delete from user" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void selectProduitNotExistingShouldReturn0() {

		int iu = 1;
		int ip = 1;

		int result = service.select(iu, ip);

		assertEquals(0, result);

	}

	@Test
	@Sql(statements = { "delete from produit",
			"insert into produit (id,name,prix,description) values (1,'Ordinateur',250,'Un produit parfait')",
			"Delete from user",
			"Insert into user (nom,prenom,email,password,age,ville) values('ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')",
			"update produit set id_user=1 where id=1" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "delete from produit", "delete from user" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void afficherProduitSelectShouldReturnListProduitSelected() {

		int iu = 1;

		List<Produit> result = service.afficherProduitSelect(iu);

		assertEquals(1, result.get(0).getId());
		assertEquals("Ordinateur", result.get(0).getName());
		assertEquals(250, result.get(0).getPrix());
		assertEquals("Un produit parfait", result.get(0).getDescription());

	}

}
