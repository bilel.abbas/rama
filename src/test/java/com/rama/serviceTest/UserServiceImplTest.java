package com.rama.serviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.rama.RamaApplication;
import com.rama.entity.User;
import com.rama.service.IUserService;

/**
 * @author bilel
 *
 */
@SpringBootTest(classes = RamaApplication.class)
public class UserServiceImplTest {

	@Autowired
	private IUserService service;

	@Test
	public void ramaApplicationMain() {

		RamaApplication.main(new String[] {});
		boolean test = true;
		assertTrue(test);

	}

	@Test
	@Sql(statements = "delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveUserNotExistingShouldReturnAdmin() {

		User user = new User("ABBAS", "Bilel", "bilel.abbas@gmail.com", "azerty", 25, "Grenoble");

		User result = service.save(user);

		assertNotNull(result);
		assertEquals("ABBAS", result.getNom());
		assertEquals("Bilel", result.getPrenom());
		assertEquals("bilel.abbas@gmail.com", result.getEmail());
		assertEquals("azerty", result.getPassword());
		assertEquals(25, result.getAge());
		assertEquals("Grenoble", result.getVille());

	}

	@Test
	@Sql(statements = { "Delete from user",
			"Insert into user (nom,prenom,email,password,age,ville) values('ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveUserExistingShouldReturnNull() {

		User user = new User("ABBAS", "Bilel", "bilel.abbas@gmail.com", "azerty", 25, "Grenoble");
		User result = service.save(user);

		assertNull(result);
	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveUserNullShouldReturnIllegalArgumentException() {

		User user = null;

		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			service.save(user);
		});
		assertEquals("probe is marked non-null but is null", exception.getMessage());

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveUserWithNameNullShouldReturnDataIntegrityViolationException() {

		User user = new User(1, null, "Bilel", "bilel.abbas@gmail.com", "azerty", 25, "Grenoble");

		Exception exception = assertThrows(DataIntegrityViolationException.class, () -> {
			service.save(user);
		});

		assertEquals(
				"could not execute statement; SQL [n/a]; constraint [null]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement",
				exception.getMessage());

	}

	@Test
	@Sql(statements = { "Delete from user",
			"insert into user (id,nom,prenom,email,password,age,ville) values (1,'ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveUserWithSameEmailShouldReturnDataIntegrityViolationException() {

		User user = new User(2, "Jabilot", "Marc", "bilel.abbas@gmail.com", "adaming", 22, "Lyon");

		Exception exception = assertThrows(DataIntegrityViolationException.class, () -> {
			service.save(user);
		});

		assertEquals(DataIntegrityViolationException.class, exception.getClass());

	}

	@Test
	@Sql(statements = { "Delete from user",
			"Insert into user(id,nom,prenom,email,password,age,ville) values (1,'ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateUserExistingShouldReturnUser() {

		User user = new User(1, "ABBAS", "Bilel", "bilel-ab@live.fr", "azerty", 25, "Grenoble");

		User result = service.update(user);

		assertNotNull(result);
		assertEquals("bilel-ab@live.fr", result.getEmail());

	}

	@Test
	@Sql(statements = { "Delete from user",
			"Insert into user(id,nom,prenom,email,password,age,ville) values (1,'ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateUserExistingwithNameNullShouldReturnDataViolationException() {

		User user = new User(1, null, "Bilel", "bilel-ab@live.fr", "azerty", 25, "Grenoble");

		Exception exception = assertThrows(DataIntegrityViolationException.class, () -> {
			service.update(user);
		});

		assertEquals(
				"could not execute statement; SQL [n/a]; constraint [null]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement",
				exception.getMessage());

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateUserNotExistingShouldReturnNull() {

		User user = new User(1, "ABBAS", "Bilel", "bilel.abbas@gmail.com", "azerty", 25, "Grenoble");

		User result = service.update(user);

		assertNull(result);
	}

	@Test
	@Sql(statements = { "Delete from user",
			"Insert into user(id,nom,prenom,email,password,age,ville) values (1,'ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteUserExistingShouldReturnTrue() {

		boolean result = service.delete(1);
		assertTrue(result);

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteUserNotExistingShouldReturnFalse() {

		boolean result = service.delete(1);

		assertFalse(result);

	}

	@Test
	@Sql(statements = { "Delete from user",
			"insert into user (id,nom,prenom,email,password,age,ville) values (1,'ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllUsersShouldReturnListNotEmpty() {

		List<User> result = service.readAll();

		assertNotNull(result);
		assertEquals(1, result.get(0).getId());
		assertEquals("ABBAS", result.get(0).getNom());
		assertEquals("Bilel", result.get(0).getPrenom());
		assertEquals("bilel.abbas@gmail.com", result.get(0).getEmail());
		assertEquals("azerty", result.get(0).getPassword());
		assertEquals(25, result.get(0).getAge());
		assertEquals("Grenoble", result.get(0).getVille());

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllUsersShouldReturnListEmpty() {

		List<User> result = service.readAll();

		assertNotNull(result);
		assertEquals("[]", result.toString());

	}

	@Test
	@Sql(statements = { "Delete from user",
			"insert into user (id,nom,prenom,email,password,age,ville) values (1,'ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void loginUserExistingShouldReturnUser() {

		User result = service.login("bilel.abbas@gmail.com", "azerty");

		assertNotNull(result);
		assertEquals(1, result.getId());
		assertEquals("ABBAS", result.getNom());
		assertEquals("Bilel", result.getPrenom());
		assertEquals("bilel.abbas@gmail.com", result.getEmail());
		assertEquals("azerty", result.getPassword());
		assertEquals(25, result.getAge());
		assertEquals("Grenoble", result.getVille());

	}

	@Test
	@Sql(statements = { "Delete from user",
			"insert into user (id,nom,prenom,email,password,age,ville) values (1,'ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void loginUserExistingWithEmailFalseShouldReturnNull() {

		User result = service.login("bilel-ab@live.fr", "azerty");

		assertNull(result);

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void loginUserNotExistingShouldReturnNull() {

		User result = service.login("bilel.abbas@gmail.com", "azerty");

		assertNull(result);
	}

	@Test
	@Sql(statements = { "Delete from user",
			"insert into user (id,nom,prenom,email,password,age,ville) values (1,'ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void searchByIdWithEmailExistingShouldReturnUser() {

		User result = service.searchById("bilel.abbas@gmail.com");

		assertNotNull(result);
		assertEquals(1, result.getId());
		assertEquals("ABBAS", result.getNom());
		assertEquals("Bilel", result.getPrenom());
		assertEquals("bilel.abbas@gmail.com", result.getEmail());
		assertEquals("azerty", result.getPassword());
		assertEquals(25, result.getAge());
		assertEquals("Grenoble", result.getVille());

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void searchByIdWithUserNotExistingShouldReturnNull() {

		User result = service.searchById("bilel.abbas@gmail.com");

		assertNull(result);

	}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void RegistrationWithEmailNotExistingShouldReturnUser() {

		User user = new User(1, "ABBAS", "Bilel", "bilel.abbas@gmail.com", "azerty", 25, "Grenoble");

		User result = service.register(user);

		assertNotNull(result);
		assertEquals("ABBAS", result.getNom());
		assertEquals("Bilel", result.getPrenom());
		assertEquals("bilel.abbas@gmail.com", result.getEmail());
		assertEquals("azerty", result.getPassword());
		assertEquals(25, result.getAge());
		assertEquals("Grenoble", result.getVille());

	}

	@Test
	@Sql(statements = { "Delete from user",
			"insert into user (id,nom,prenom,email,password,age,ville) values (1,'ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void RegistrationWithEmailExistingShouldReturnNull() {

		User user = new User(2, "JABILOT", "Marc", "bilel.abbas@gmail.com", "1234", 22, "Lyon");

		User result = service.register(user);
		assertNull(result);

	}

//	@Test
//	@Sql(statements = { "Delete from user",
//			"Insert into user (nom,prenom,email,password,age,ville) values('ABBAS','Bilel','bilel.abbas@gmail.com','azerty',25,'Grenoble')" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
//	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
//	public void readByIdUserExistingShouldReturnNotNull() {
//
//		int id = 1;
//		User result = service.readById(id);
//
//		assertNotNull(result);
//		assertEquals("ABBAS", result.getNom());
//		assertEquals("Bilel", result.getPrenom());
//		assertEquals("bilel.abbas@gmail.com", result.getEmail());
//		assertEquals("azerty", result.getPassword());
//		assertEquals(25, result.getAge());
//		assertEquals("Grenoble", result.getVille());
//		}

	@Test
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Delete from user", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdUserNullShouldReturnIllegalArgumentException() {

		int id = 1;
		User result = service.readById(id);

		assertNull(result);

	}

}
